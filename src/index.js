import React from 'react';
import ReactDOM from 'react-dom';
import 'element-theme-default';
// import 'minireset.css';
// import 'material-design-lite/dist/material.amber-blue.min.css';
import './assets/css/index.css';

import * as serviceWorker from './serviceWorker';

import { FireContextProvider } from './contexts/FireContext.jsx';
import App from './App.jsx';

ReactDOM.render(<FireContextProvider><App /></FireContextProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
serviceWorker.register();
