import React, { useContext } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Loading, Message } from 'element-react';

import { FireContext, fireAuth, usersRef } from './contexts/FireContext.jsx';

import AddRecipe from './layouts/AddRecipe';
import Index from './layouts/Index';
import SignIn from './layouts/SignIn.jsx';
import Recipes from './layouts/Recipes.jsx';

import Header from './partials/Header.jsx';

let userListenersAttached = false;

const App = () => {

  const { user, setUser, attachUserListeners } = useContext(FireContext);

  fireAuth.onAuthStateChanged(userUpdate => {
    if (userUpdate && (!user || user.local) && !userListenersAttached) {
      userListenersAttached = true;
      attachUserListeners(userUpdate.uid);
    }
  });

  const localUser = localStorage.getItem('localUser') && !user
    ? JSON.parse(localStorage.getItem('localUser'))
    : null;

  if (localUser) setUser({ ...localUser, local: true });

  return (
    <div className="app">
      <BrowserRouter>
        <Header />
        {user ?
          (
            <Switch>
              <Route exact path="/" component={Index} />
              <Route exact path="/recipes" component={Recipes} />
              <Route exact path="/recipes/add" component={AddRecipe} />
              <Route exact path="/recipes/:id" component={AddRecipe} />
              <Route exact path="/checklist" component={Index} />
              <Route path="/checklists/:id" component={Index} />
              <Route exact path="/user"><Redirect to="/" /></Route>
            </Switch>
          ) : (
            <Switch>
              <Route exact path="/user" component={SignIn} />
              <Route path="/"><Redirect to="/user" /></Route>
              {/* <Route path="/:id"><Redirect to="/user" /></Route> */}
            </Switch>
          )}
      </BrowserRouter>
    </div>
  );
}

export default App;
