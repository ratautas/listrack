import React, { useState, useEffect, useContext, useRef } from 'react';
import { deburr, capitalize, differenceBy } from 'lodash';

import { FireContext, productsRef } from '../contexts/FireContext';

const AddProduct = props => {

  const { fireProducts } = useContext(FireContext);

  const [product, setProduct] = useState({ entry: '', query: '', quantity: 1, unit: 'vnt', });
  const [filteredProducts, setFilteredProducts] = useState(fireProducts ? [...fireProducts] : []);
  const [searchActive, setSearchActive] = useState(false);

  const $input = useRef(null);

  const resetProduct = () => setProduct({ entry: '', query: '', quantity: 1, unit: 'vnt', })

  const addProduct = product => {
    resetProduct();
    props.onProductAdd(product);
    if (props.onProductAdded) props.onProductAdded();
  }

  const handleProductSubmit = e => {
    e.preventDefault();
    if (!product.entry.length) return;
    if (filteredProducts.length) addProduct(filteredProducts[0]);
    else createNewProduct();
  };

  const activateSearch = () => setSearchActive(true);

  const deactivateSearch = () => {
    resetProduct();
    setSearchActive(false);
  };

  const createNewProduct = () => {
    const { entry, quantity, unit } = product;
    const title = capitalize(entry);
    productsRef.add({ title, quantity, unit }).then(product => {
      addProduct({ title, quantity, unit, id: product.id });
    });
  };

  useEffect(() => {
    if (fireProducts && fireProducts.length) {
      const filtered = fireProducts.filter(p => deburr(p.title).toLowerCase().includes(product.query));
      const filteredWithoutAdded = differenceBy(filtered, props.excludedProducts, 'id');
      setFilteredProducts(filteredWithoutAdded);
    }
  }, [fireProducts, product, props.excludedProducts]);

  useEffect(() => {
    const { query, entry } = product;
    const updatedQuery = deburr(entry).toLowerCase();
    if (query !== updatedQuery) setProduct({ ...product, query: updatedQuery });
  }, [product]);

  const handleInputChange = e => {
    const { name, value } = e.target;
    setProduct({ ...product, [name]: value });
  };

  return (
    <>
      <div className="">
        <form className="" onSubmit={e => handleProductSubmit(e)}>
          <div className="">
            <div className=" field">
              <span className="field__close" onClick={deactivateSearch}>
                <b>X</b>
              </span>
              <input className="field__input"
                name="entry"
                type="text"
                placeholder={props.placeholder}
                value={product.entry}
                ref={$input}
                onChange={handleInputChange}
                onFocus={activateSearch}
              />
              {searchActive && product.entry.length > 0 && (
                <button className="btn">
                  {filteredProducts.length > 0 ? props.addLabel : props.createLabel}
                </button>
              )}
            </div>
            {searchActive && product.entry.length > 0 && (
              <div className="">
                <input className="field__input"
                  name="quantity"
                  type="number"
                  inputMode="numeric"
                  placeholder={props.placeholder}
                  value={product.quantity}
                  onChange={handleInputChange}
                />
                <select className="field__input"
                  name="unit"
                  onChange={handleInputChange}
                  value={product.unit}
                >
                  <option value="vnt">vnt.</option>
                  <option value="pag">pak.</option>
                  <option value="g">g.</option>
                </select>
              </div>
            )}
          </div>
        </form>
        {searchActive && (
          <>
            {filteredProducts.length > 0 ?
              (
                filteredProducts.map((product, p) => {
                  return (
                    <div key={p}
                      className={`product ${(p === 0 && product.entry && product.entry.length) ? ' activestate' : ''}`}
                      onClick={() => addProduct(product)}
                    >
                      {product.title}
                    </div>
                  )
                })
              ) : (
                <>
                  {product.entry.length > 0 && (
                    <div className="ss activestate" onClick={createNewProduct}>{capitalize(product.entry)}</div>
                  )}
                </>
              )
            }
          </>
        )}
      </div>
    </>
  )
}

export default AddProduct;