import React, { useContext, useState, useRef, useEffect } from 'react';
import getMoment from '../utilities/getMoment';

import { FireContext, checklistsRef } from '../contexts/FireContext';

import AddProduct from '../components/AddProduct';

const Memo = props => {

  const { user, updateUser, fireChecklists } = useContext(FireContext);

  const { uid, displayName } = user;

  const [moveActive, setMoveActive] = useState(false);
  const [moveToTitle, setMoveToTitle] = useState('New Checklist');

  const $moveInput = useRef(null);

  const updateMemoProducts = products => updateUser({ memo: { products } });

  const removeProductFromMemo = id => updateMemoProducts(user.memo.filter(p => p.id !== id));

  const createChecklist = e => {

    e.preventDefault();

    if (user.memo && user.memo.length > 0 && moveToTitle.length > 0) {
      const { date, stamp } = getMoment();
      const checklistToCreate = {
        addedDate: date,
        addedStamp: stamp,
        products: user.memo,
        title: moveToTitle,
        user: { uid, displayName },
      };

      checklistsRef.add(checklistToCreate).then(createdChecklist => {
        updateUserChecklists({ ...checklistToCreate, id: createdChecklist.id });
      });
    }

  };

  const mergeChecklist = id => {
    if (user.memo && user.memo.length > 0 && moveToTitle.length > 0) {
      const checklistToMerge = fireChecklists.find(checklist => checklist.id === id);
      checklistToMerge.products = [...user.memo, ...checklistToMerge.products];

      checklistsRef.doc(id).set(checklistToMerge, { merge: true })
        .then(() => updateUserChecklists(checklistToMerge));
    }
  };

  const updateUserChecklists = checklist => {
    updateUser({ checklists: [{ ...checklist }, ...user.checklists ? user.checklists : []] });
    updateMemoProducts([]);
  };

  const addProductToMemo = product => updateUser({
    memo: [
      {
        ...product,
        user: { uid, displayName },
      },
      ...user.memo && user.memo.length ? user.memo : []
    ]
  })

  useEffect(() => {
    if (moveActive) setTimeout(() => $moveInput.current.select(), 1);
  }, [moveActive])

  return (
    <div className={`memo ${props.className ? props.className : ''}`}>{/* Memo */}
      <div className="">{/* Memo Top */}
        <div className=""> {/* Memo Head */}
          <div className="">
            <h2>Memo</h2>
          </div>
          {user.memo && user.memo.length > 0 && (
            <>
              <button className="" onClick={() => setMoveActive(true)}>Move To New Checklist</button>
              {moveActive && (
                <form className="" onSubmit={createChecklist}>
                  <div className="">
                    <div className="" onClick={() => setMoveActive(false)}>X</div>
                    <input className="field__input"
                      type="text"
                      placeholder="new lists title"
                      value={moveToTitle}
                      ref={$moveInput}
                      onChange={e => setMoveToTitle(e.target.value)}
                    />
                    {moveToTitle.length > 0 && (
                      <button className="">Move</button>
                    )}
                  </div>
                  {user.checklists && user.checklists.length > 0 && (
                    user.checklists.map(checklist => {
                      return (
                        <div className=""
                          key={checklist.id}
                          onClick={() => mergeChecklist(checklist.id)}>
                          {checklist.title}
                        </div>
                      )
                    })
                  )}
                </form>
              )}
            </>
          )}
        </div>
        <div className=""> {/* Memo Actions */}
          <AddProduct
            className=""
            addLabel="Add"
            createLabel="Create"
            placeholder="Add New Product"
            excludedProducts={user.memo}
            onProductAdd={addProductToMemo}
          />
        </div>
      </div>
      <div className=""> {/* Memo List */}
        {user.memo && user.memo.length > 0 &&
          user.memo.map(product => {
            return (
              <div className="" key={product.id}> {/* Memo Item */}
                <div className=""> {/* Memo Item Title */}
                  <div className="">{product.title}</div> {/* Memo Item Label */}
                  <div className="">{product.addedDate} by {product.user.displayName}</div> {/* Memo Item Meta */}
                </div>
                <div className=""> {/* Memo Item Actions */}
                  <button className="" onClick={() => removeProductFromMemo(product.id)}>Remove</button> {/* Memo Item Action */}
                </div>
              </div>
            )
          })}
      </div>
    </div>
  )
}

export default Memo;