import React, { useContext, useState } from 'react';

import { FireContext, checklistRef } from '../contexts/FireContext';

import AddProduct from '../components/AddProduct';

const Checklist = props => {

  const { user, updateUser } = useContext(FireContext);

  const [products, setProducts] = useState(
    props.checklist && props.checklist.products ? props.checklist.products : []
  );

  const updateProducts = products => {
    setProducts(products);
    // updateUser({ checklist: { products } });
  };

  const removeProductFromChecklist = id => updateProducts(products.filter(p => p.id !== id));

  const createNew = () => { };

  const addProductToChecklist = product => {
    updateProducts([{
      ...product,
      user: {
        uid: user.uid,
        displayName: user.displayName
      },
      addedOn: Date.now()
    }, ...products]);
  };

  return (
    <div className={`checklist ${props.className ? props.className : ''}`}>{/* Checklist */}
      <div className="">{/* Checklist Top */}
        <div className=""> {/* Checklist Head */}
          <div className="">
            <h3>{props.checklist.title}</h3>
            </div>
          <button className="" onClick={createNew}>Create New</button>
        </div>
        <div className=""> {/* Checklist Actions */}
          <AddProduct
            className=""
            addLabel="Add"
            createLabel="Create"
            placeholder="Add Product"
            excludedProducts={products}
            onProductAdd={addProductToChecklist}
          />
        </div>
      </div>
      <div className=""> {/* Checklist List */}
        {products.length > 0 &&
          products.map(product => {
            return (
              <div className="" key={product.id}> {/* Checklist Item */}
                <div className=""> {/* Checklist Item Title */}
                  <div className="">{product.title}</div> {/* Checklist Item Label */}
                  <div className="">{product.addedDate} by {product.user.displayName}</div> {/* Checklist Item Meta */}
                </div>
                <div className=""> {/* Checklist Item Actions */}
                  <button className="" onClick={() => removeProductFromChecklist(product.id)}>Remove</button> {/* Checklist Item Action */}
                </div>
              </div>
            )
          })}
      </div>
    </div>
  )
}

export default Checklist;