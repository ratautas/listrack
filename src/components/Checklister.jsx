import React, { useContext, useState } from 'react';

import { FireContext } from '../contexts/FireContext';

import Checklist from '../components/Checklist';

const Checklister = props => {

  const { user, updateUser } = useContext(FireContext);
  const [createNewActive, setCreateNewActive] = useState(false);

  const activateCreateNew = () => {
    setCreateNewActive(true);
  };

  const deactivateCreateNew = () => {
    setCreateNewActive(false);
  };

  const updateChecklists = checklists => {
    // setChecklists(checklists);
    updateUser({ checklists });
  };

  // const removeChecklistFromChecklister = id => updateChecklists(checklists.filter(p => p.id !== id));

  // const addChecklistToChecklister = checklist => {
  //   updateChecklists([{
  //     ...checklist,
  //     user: {
  //       uid: user.uid,
  //       displayName: user.displayName
  //     },
  //     addedOn: Date.now()
  //   }, ...checklists]);
  // };

  const handleInputChange = e => {
    const { name, value } = e.target;
  };

  return (
    <div className={`checklister ${props.className ? props.className : ''}`}>{/* Checklister */}
      <div className="">{/* Checklister Top */}
        <div className=""> {/* Checklister Head */}
          <div className=""><h2>Checklister</h2></div>
          <button className="" onClick={activateCreateNew}>Create New Checklist</button>
        </div>
        {createNewActive && (
          <div className=""> {/* Checklister Actions */}
            <div className=" field">
              <button className="field__close" onClick={deactivateCreateNew}>
                <b>X</b>
              </button>
              <input className="field__input"
                name="entry"
                type="text"
                placeholder="Checklist title"
                // value={product.entry}
                // ref={$input}
                onChange={handleInputChange}
              // onFocus={activateSearch}
              />
            </div>
          </div>
        )}
      </div>
      <div className=""> {/* Checklister Checklist */}
        {user.checklists && user.checklists.length > 0 &&
          user.checklists.map(checklist => <Checklist key={checklist.id} checklist={checklist} inEdit={false} />)
        }
      </div>
    </div>
  )
}

export default Checklister;