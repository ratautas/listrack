// https://benmcmahen.com/using-firebase-with-react-hooks/
import React, { createContext, useState, useEffect } from 'react';
import { useCollectionData } from 'react-firebase-hooks/firestore';

import * as firebase from 'firebase';

import 'firebase/auth';
import 'firebase/firestore';

export const fireConfig = {
  apiKey: "AIzaSyBiXVFe64h4LhCu9O3xmfKUv44E_PcjU94",
  authDomain: "react-native-list-258404.firebaseapp.com",
  databaseURL: "https://react-native-list-258404.firebaseio.com",
  projectId: "react-native-list-258404",
  storageBucket: "react-native-list-258404.appspot.com",
  messagingSenderId: "493939147371",
  appId: "1:493939147371:web:caa77300a2dc40233d272e",
  measurementId: "G-DC74Y44BFC"
};

// Initialize Firebase
export const firebaseApp = firebase.initializeApp(fireConfig);

export const FireContext = createContext();

export const fireStore = firebase.firestore();
export const fireAuth = firebase.auth();

export const checklistsRef = fireStore.collection('checklists');
export const recipesRef = fireStore.collection('recipes');
export const productsRef = fireStore.collection('products');
export const usersRef = fireStore.collection('users');


export const FireContextProvider = (props) => {

  const [user, setUser] = useState(null);

  const [fireChecklists] = useCollectionData(checklistsRef, { idField: 'id' });
  const [fireProducts] = useCollectionData(productsRef, { idField: 'id' });
  const [fireRecipes] = useCollectionData(recipesRef, { idField: 'id' });
  const [fireUsers] = useCollectionData(usersRef, { idField: 'id' });

  const attachUserListeners = (id) => {
    usersRef.doc(id).onSnapshot(doc => {
      console.log('get user data');
      const userData = doc.data();
      setUser({ ...userData, uid: doc.id, local: false });
      localStorage.setItem('localUser', JSON.stringify(userData));
    });
  };

  const updateUser = updateData => {
    const userWithUpdates = { ...user, ...updateData };
    usersRef.doc(user.uid).set({ ...updateData }, { merge: true });
    setUser(userWithUpdates);
    localStorage.setItem('localUser', JSON.stringify(userWithUpdates));
  };

  const mergeLists = (list1, list2) => {
    return [...list1, ...list2];
  };

  return (
    <FireContext.Provider value={{
      user,
      setUser,
      updateUser,
      attachUserListeners,

      recipesRef,
      checklistsRef,
      productsRef,

      fireChecklists,
      fireProducts,
      fireRecipes,
      fireUsers,
    }}>
      {props.children}
    </FireContext.Provider>
  );
}

export const withFirebaseHOC = Component => props => (
  <FireContext.Consumer>
    {state => <Component {...props} firebase={state} />}
  </FireContext.Consumer>
)