// https://benmcmahen.com/using-firebase-with-react-hooks/
import React, { createContext, useState, useEffect } from 'react';
// import { useAuthState } from 'react-firebase-hooks/auth';
import { useCollectionData } from 'react-firebase-hooks/firestore';
import { values } from 'lodash';

import * as firebase from 'firebase';
import * as firebaseui from 'firebaseui';

import 'firebase/auth';
import 'firebase/firestore';

import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
// import FirebaseAuth from 'react-firebaseui/FirebaseAuth';

export const fireConfig = {
  apiKey: "AIzaSyBiXVFe64h4LhCu9O3xmfKUv44E_PcjU94",
  authDomain: "react-native-list-258404.firebaseapp.com",
  databaseURL: "https://react-native-list-258404.firebaseio.com",
  projectId: "react-native-list-258404",
  storageBucket: "react-native-list-258404.appspot.com",
  messagingSenderId: "493939147371",
  appId: "1:493939147371:web:caa77300a2dc40233d272e",
  measurementId: "G-DC74Y44BFC"
};

// Initialize Firebase
export const firebaseApp = firebase.initializeApp(fireConfig);

export const FireContext = createContext();

export const fireStore = firebase.firestore();

export const usersRef = fireStore.collection('users');
export const recipesRef = fireStore.collection('recipes');
export const listersRef = fireStore.collection('listers');
export const productsRef = fireStore.collection('products');

export const fireAuth = firebase.auth();
export const fireUIConfig = {
  // Popup signin flow rather than redirect flow.
  // signInFlow: 'popup',
  // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
  signInSuccessUrl: '/signedIn',
  // We will display Google and Facebook as auth providers.
  signInOptions: [
    firebase.auth.EmailAuthProvider.PROVIDER_ID,
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.FacebookAuthProvider.PROVIDER_ID
  ],
  // callbacks: {
  //   // Avoid redirects after sign-in.
  //   signInSuccessWithAuthResult: () => false
  // }
};

export const fireUI = new firebaseui.auth.AuthUI(fireAuth);

// fireUI.start('#firebaseui-auth-container', {
//   signInOptions: [
//     firebase.auth.EmailAuthProvider.PROVIDER_ID
//   ],
//   // Other config options...
// });

export const FireContextProvider = (props) => {

  const [user, setUser] = useState(null);
  const [errorBag, setErrorBag] = useState({});
  const [userData, setUserData] = useState({});

  // const [user] = useAuthState(fireAuth);
  const [fireProducts] = useCollectionData(productsRef, { idField: 'id' });
  const [fireRecipes] = useCollectionData(recipesRef, { idField: 'id' });
  const [fireUsers] = useCollectionData(usersRef, { idField: 'id' });

  const clearErrorBag = () => setErrorBag({});

  // const collectUserData = ref => {
  //   if (typeof userData[ref] === 'undefined' && user) {
  //     // usersRef.doc(`${user.uid}`).collection(ref).onSnapshot(snapshot => {
  //     usersRef.doc(`${user.uid}`).collection(ref).get().then(snapshot => {
  //       setUserData({
  //         ...userData,
  //         [ref]: snapshot.docs.reduce((acc, doc) => {
  //           return { ...acc, [doc.id]: doc.data() }
  //         }, {})
  //       })
  //     })
  //   }
  // };

  const loginWithEmail = (email, password) => {
    fireAuth
      .signInWithEmailAndPassword(email, password)
      .catch(error => setErrorBag({ login: error.message }));
  };

  const registerWithEmail = (email, password) => {
    fireAuth
      .createUserWithEmailAndPassword(email, password)
      .then(user => usersRef.doc(user.user.uid).set({ email: user.user.email }))
      .catch(error => setErrorBag({ registration: error.message }));
  };

  // const fireCreateProduct = async product => {
  //   productsRef
  //     .add(product)
  //     .then(addedProduct => {
  //       return addedProduct;
  //       // usersRef.doc(`${user.uid}`).collection('products').add({ ...product });
  //     })
  //     .catch(error => setErrorBag({ fireCreateProduct: error.message }));
  // };

  // const fireCreateProduct = async product => await productsRef.add(product);
  const fireCreateProduct = product => productsRef.add(product)
    .catch(error => setErrorBag({ fireCreateProduct: error.message }));

  // const fireCreateRecipe = async recipe => {
  //   return await recipesRef.add(recipe);
  // }

  const fireCreateRecipe = recipe => {
    recipesRef.add(recipe).then(addedRecipe => {
      usersRef.doc(user.uid).collection('recipes').add({ ...addedRecipe }).then(addedUserRecipe => {
        values(recipe.products).forEach(product => {
          recipesRef.doc(addedRecipe.id).collection('products').add({ ...product });
          // usersRef.doc(user.uid).collection('recipes').add({ ...addedRecipe }).then(addedUserRecipe => {
          //   recipesRef.doc(addedRecipe.id).collection('products').add({ ...product });
          // });
        });
      })
    })
  };

  const logOut = () => {
    setErrorBag({});
    fireAuth
      .signOut()
      .catch(error => setErrorBag({ logout: error.message }));
  }

  fireAuth.onAuthStateChanged(user => {
    if (user) {
      // collectUserData('recipes');
      // collectUserData('products');
      // collectUserData('listers');
    }
  });


  return (
    <FireContext.Provider value={{
      user,
      setUser,
      loginWithEmail,
      logOut,
      registerWithEmail,
      clearErrorBag,
      errorBag,
      recipesRef,
      listersRef,
      productsRef,
      fireCreateProduct,
      fireCreateRecipe,
      fireProducts,
      fireRecipes,
      fireUsers,
      userData,
    }}>
      {props.children}
    </FireContext.Provider>
  );
}

export const withFirebaseHOC = Component => props => (
  <FireContext.Consumer>
    {state => <Component {...props} firebase={state} />}
  </FireContext.Consumer>
)