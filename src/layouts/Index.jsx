import React, { useContext, useState, useEffect } from 'react';

import { FireContext } from '../contexts/FireContext';

import Memo from '../components/Memo';
import Checklister from '../components/Checklister';

const Index = () => {

  const { user, setUser, updateUser } = useContext(FireContext);

  const addNewChecklist = () => {
    console.log('hi');
  }

  return (
    <div className="">
      <Memo />
      <Checklister />
    </div>
  )
}

export default Index;