import React, { useState, useContext } from 'react';
import { FireContext } from '../contexts/FireContext';
import { NavLink } from 'react-router-dom';

const Login = () => {

  const [user, setUser] = useState({ email: '', password: '' });

  const { errorBag, loginWithEmail, clearErrorBag, fireRecipes } = useContext(FireContext);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const { email, password } = user;
    clearErrorBag();
  }

  return (
    <>rn
      <div className="container">
        <h2>INDEX (Dashboard)</h2>
        <h4>
          <NavLink to="/recipes" className="">RECIPES</NavLink>&nbsp;
          <b><NavLink to="/recipes/add" className="">(ADD NEW)</NavLink></b>
          {fireRecipes && fireRecipes.length && fireRecipes.map((recipe, r) => {
            return <div className="" key={r}>{recipe.title}</div>
          })}
        </h4>
      </div>
    </>
  )
}

export default Login;