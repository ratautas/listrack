import React, { useState, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { Tabs, Input, Button, Loading, Message } from 'element-react';

import { FireContext, fireAuth, usersRef } from '../contexts/FireContext';

const SignIn = () => {

  const [fields, setFields] = useState({ email: '', password: '', displayName: '' });
  const [loginError, setLoginError] = useState(null);
  const [registrationError, setRegistrationError] = useState(null);
  const [isLoading, setLoading] = useState(false);

  const { user, setUser } = useContext(FireContext);

  const handleInput = e => {
    const { name, value } = e.target;
    setLoginError(false);
    setRegistrationError(false);
    setFields({ ...fields, [name]: value });
  }

  const handleLogin = e => {
    e.preventDefault();
    setLoading(true);
    setLoginError(null);
    fireAuth.signInWithEmailAndPassword(fields.email, fields.password)
      .then(loggedInUser => usersRef.doc(loggedInUser.user.uid).get().then(doc => {
        const userData = doc.data();
        setLoading(false);
        setUser(userData);
        localStorage.setItem('localUser', JSON.stringify(userData));
        Message({
          showClose: true,
          message: `${userData.displayName} has signed in successfully`,
          type: 'success'
        });
        return <Redirect to="/" />;
      }))
      .catch(e => {
        setLoading(false);
        setLoginError(e.message);
      });
  }

  const handleRegister = e => {
    e.preventDefault();
    setRegistrationError(null);
    setLoading(true);
    fireAuth
      .createUserWithEmailAndPassword(fields.email, fields.password)
      .then(createdUser => usersRef.doc(createdUser.user.uid).set({ ...fields }).then(() => {
        setLoading(false);
        setUser({ ...fields });
        Message({
          showClose: true,
          message: `${fields.displayName} has signed up successfully`,
          type: 'success'
        });
        localStorage.setItem('localUser', JSON.stringify({ ...fields }));
        return <Redirect to="/" />;
      }))
      .catch(e => {
        setLoading(false);
        setRegistrationError(e.message);
      });
  }

  return (
    <>
      <Tabs activeName="login" onTabClick={(tab) => console.log(tab.props.name)}>
        <Tabs.Pane label="Login" name="login">
          <form className="" onSubmit={handleLogin}>
            <div className="">
              <label className=" field">
                <input className="field__input" type="email" placeholder="test@mailbox.com"
                  name="email" value={fields.email} onChange={handleInput} />
              </label>
            </div>
            <div className="">
              <label className=" field">
                <input className="field__input" type="password" placeholder="Your pass"
                  name="password" value={fields.password} onChange={handleInput} />
              </label>
            </div>
            <div className="">
              <button className="btn">LOGIN</button>
            </div>
            {loginError && <div>{loginError}</div>}
          </form>
        </Tabs.Pane>
        <Tabs.Pane label="Register" name="register">
          <form className="" onSubmit={handleRegister}>
            <div className="">
              <label className=" field">
                <input className="field__input" type="text" placeholder="Your name"
                  name="displayName" value={fields.displayName} onChange={handleInput} />
              </label>
            </div>
            <div className="">
              <label className=" field">
                <input className="field__input" type="email" placeholder="test@mailbox.com"
                  name="email" value={fields.email} onChange={handleInput} />
              </label>
            </div>
            <div className="">
              <label className=" field">
                <input className="field__input" type="password" placeholder="Your pass"
                  name="password" value={fields.password} onChange={handleInput} />
              </label>
            </div>
            <div className="">
              <Button icon="edit" size="large" type="primary" onClick={handleRegister}>REGISTER</Button>
            </div>
          </form>
          {registrationError && <div>{registrationError}</div>}
        </Tabs.Pane>
      </Tabs>
      {isLoading && <Loading fullscreen={true} />}
    </>
  )
}

export default SignIn;