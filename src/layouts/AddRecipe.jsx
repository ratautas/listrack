import React, { useState, useContext, useRef } from 'react';
import { values } from 'lodash';

import { FireContext } from '../contexts/FireContext';
// import { reverse } from 'lodash';

const AddRecipe = () => {

  const [recipe, setRecipe] = useState({
    title: '',
    products: [],
    text: '',
    duration: '',
    serves: ''
  });

  const [addProductActive, setAddProductActive] = useState(false);

  const { fireCreateRecipe } = useContext(FireContext);

  const controlAddProduct = async asyncProduct => {
    setAddProductActive(false);
    const product = await asyncProduct;
    // setRecipe({
    //   ...recipe,
    //   products: [
    //     {
    //       title: product.title,
    //       id: product.id,
    //       quantity: '',
    //       autoAdd: true
    //     },
    //     ...recipe.products
    //   ]
    // });
    setRecipe({
      ...recipe,
      products: {
        [product.id]: {
          title: product.title,
          id: product.id,
          quantity: '',
          autoAdd: true
        },
        ...recipe.products
      }
    });
  }

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setRecipe({ ...recipe, [name]: value });
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    fireCreateRecipe(recipe);
  }

  const handleQuantityChange = (e, i) => {
    recipe.products[i].quantity = e.target.value;
    setRecipe({ ...recipe });
  }

  const handleAutoAddClick = i => {
    recipe.products[i].autoAdd = !recipe.products[i].autoAdd;
    setRecipe({ ...recipe });
  }

  return (
    <>
      <h2>ADD RECIPE</h2>
      <form className="" onSubmit={handleSubmit}>
        <div className="">
          <label className=" field">
            <input className="field__input" type="text"
              placeholder="Recipe Title" name="title"
              value={recipe.title} onChange={handleInputChange}
            />
          </label>
        </div>
        <div className="">
          <div className="">Ingredients (products)</div>
          <div className="">=======================</div>
          <div className="">
            {recipe.products ? values(recipe.products).map(product => {
              return (
                <div className="" key={product.id}>
                  <div className="">{product.title}</div>
                  <input type="text" value={product.quantity}
                    placeholder="quantity"
                    onChange={e => handleQuantityChange(e, product.id)} />
                  <div className={`add ${recipe.products[product.id].autoAdd ? 'yes' : 'no'}`}
                    onClick={() => handleAutoAddClick(product.id)}>
                    O
                    </div>
                </div>
              )
            }) : <div className="ss">no ingredients yet</div>}
          </div>
          <div className="">=======================</div>
          <div className="btn" onClick={() => setAddProductActive(true)}>ADD INGREDIENT (PRODUCT)</div>
        </div>
        <div className="">
          <label className=" field">
            <span className="field__placeholder">Recipe Duration</span>
            <input className="field__input" type="text"
              placeholder="Recipe Duration" name="duration"
              value={recipe.duration} onChange={handleInputChange}
            />
          </label>
          <label className=" field">
            <span className="field__placeholder">Recipe Serves</span>
            <input className="field__input" type="text"
              placeholder="Recipe Serves" name="serves"
              value={recipe.serves} onChange={handleInputChange}
            />
          </label>
        </div>
        <div className="">
          <label className=" field">
            <span className="field__placeholder">Recipe Text</span>
            <textarea className="field__input field__input--textarea" type="text"
              placeholder="Recipe Text" name="text"
              value={recipe.text} onChange={handleInputChange}
            />
          </label>
        </div>
        <div className="">
          <button className="btn">ADD RECIPE</button>
        </div>
      </form>
    </>
  )
}

export default AddRecipe;