import React, { useState, useEffect, useContext } from 'react';
import { deburr, differenceBy } from 'lodash';

import { FireContext } from '../contexts/FireContext';

const AddProduct = props => {

  const capitalize = (string) => `${string[0].toUpperCase()}${string.slice(1)}`;

  const { fireProducts, fireCreateProduct } = useContext(FireContext);

  const [query, setQuery] = useState('');
  const [entry, setEntry] = useState('');
  const [filteredProducts, setFilteredProducts] = useState(fireProducts ? [...fireProducts] : []);

  const handleProductSubmit = e => {
    e.preventDefault();
    if (!entry.length) return;
    addProduct(filteredProducts.length ? filteredProducts[0] : createProduct(entry));
  }

  const handleProductClick = product => {
    addProduct(product);
  }

  const addProduct = async product => {
    setQuery('');
    setEntry('');
    await props.controlAddProduct(product);
  }

  const createProduct = async productTitle => {
    const title = capitalize(productTitle);
    const { id } = await fireCreateProduct({ title });
    return { id, title }
  }

  const handleInputChange = e => {
    setEntry(e.target.value);
    setQuery(deburr(e.target.value).toLowerCase());
  }

  useEffect(() => {
    if (fireProducts && fireProducts.length) {
      const filtered = fireProducts.filter(p => deburr(p.title).toLowerCase().includes(query))
      const filteredWithoutAdded = differenceBy(filtered, props.addedProducts, 'title');
      setFilteredProducts(filteredWithoutAdded);
    }
  }, [fireProducts, entry, props.addedProducts, query]);

  return (
    <>
      <div className="">
        <form className="" onSubmit={e => handleProductSubmit(e)}>
          <div className="">
            <label className=" field">
              <span className="field__placeholder">Product Title</span>
              <input className="field__input" type="text"
                placeholder="Product Title" name="title"
                value={entry} onChange={handleInputChange}
              />
            </label>
            <button className="btn">ADD</button>
          </div>
        </form>
        <h2>product list</h2>
        {filteredProducts.length ? filteredProducts.map((product, p) => {
          return (
            <div key={p} className={`ff ${p === 0 ? 'd' : ''}`} onClick={() => handleProductClick(product)}>
              {product.title}
            </div>
          )
        }) : <div className="ss">no results</div>}
      </div>
    </>
  )
}

export default AddProduct;