import React, { useContext, useState } from 'react';
import { NavLink, Redirect } from 'react-router-dom';
import { FireContext, fireAuth } from '../contexts/FireContext';

import { Menu, Message, Loading } from 'element-react';

const Header = () => {

  const { user, setUser } = useContext(FireContext);

  const [isLoading, setLoading] = useState(false);

  const onSelect = () => { };

  const logOut = () => {
    setUser(null);
    localStorage.removeItem('localUser');
    setLoading(true);
    (() => <Redirect to="/user" />)();
    fireAuth.signOut().then(() => {
      setLoading(false);
      Message({
        showClose: true,
        message: `Signed out successfully`,
        type: 'info'
      });
    });
  };

  return (
    <>
      <header className="app__header header">
        <div className="header__container container">
          <Menu theme="dark" mode="horizontal">
            {user ?
              (<>
                <Menu.Item index="4">{user.displayName}</Menu.Item>
                <div className="el-menu-item">
                  <span className="el-menu-item__trigger" onClick={logOut}>Log Out</span>
                </div>
              </>) :
              (<>
                <Menu.Item index="4" className="extra">
                  <NavLink to="/login" className="el-menu-item__trigger">Log in / Register</NavLink>
                </Menu.Item>
              </>)}
          </Menu>
        </div>
      </header>
      {isLoading && <Loading fullscreen={true} />}
    </>
  );
}

export default Header;