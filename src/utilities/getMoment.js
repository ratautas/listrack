import moment from 'moment';
export default () => {
  const now = moment();
  return {
    now,
    date: now.toObject(),
    stamp: Number(now.format('x'))
  }
};